import { OPSportfolioPage } from './app.po';

describe('opsportfolio App', () => {
  let page: OPSportfolioPage;

  beforeEach(() => {
    page = new OPSportfolioPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
