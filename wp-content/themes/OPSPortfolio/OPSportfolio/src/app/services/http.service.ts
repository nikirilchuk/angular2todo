import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { WpPost } from '../interfaces/wp-post';
import {NgSpinningPreloader} from 'ng2-spinning-preloader';

import { environment } from '../../environments/environment'

@Injectable()
export class HttpService {

	public apiurl: string;

  	constructor(private http: Http, private ngSpinningPreloader: NgSpinningPreloader) {
  		this.apiurl = environment.apiUrl;
  	}

  	getPostsList():Observable<any> {
        this.ngSpinningPreloader.start();
  		  return this.http.get(`${this.apiurl}posts`).map((resp:Response)=>
	  		    {
                this.ngSpinningPreloader.stop();
	              return resp;
	          });
  	}
  	getPost(id):Observable<any> {
      this.ngSpinningPreloader.start();
  		return this.http.get(`${this.apiurl}posts/${id}`).map((resp:Response)=>
	  		  {
              this.ngSpinningPreloader.stop();
	            return resp;
	        });
  	}
}
