import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import {NgSpinningPreloader} from 'ng2-spinning-preloader';

import { AppComponent } from './app.component';
import { HomeModule } from './modules/home/home.module';

import { HttpService } from './services/http.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpModule,
    HomeModule
  ],
  providers: [
    HttpService,
    NgSpinningPreloader
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
