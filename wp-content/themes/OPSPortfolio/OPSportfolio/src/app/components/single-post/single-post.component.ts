import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../services/http.service';

@Component({
    selector: 'app-single-post',
    templateUrl: './single-post.component.html',
    styleUrls: ['./single-post.component.scss'],
    providers: [HttpService]
})
export class SinglePostComponent implements OnInit {
    private postData:any;
    private sub:any;
    constructor(private httpservice:HttpService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let id = params['id'];
            this.httpservice.getPost(id).subscribe((data: Response) => {this.postData=data.json()});
        });
    }

    ngOnDestroy() {
        // Clean sub to avoid memory leak
        this.sub.unsubscribe();
    }

}
