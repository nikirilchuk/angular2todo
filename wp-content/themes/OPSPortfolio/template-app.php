<?php
/*
Template Name: App
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="/app/">
    <title>
        <?php wp_title( '|', 'true', 'right' );
        bloginfo( 'name' );
        ?>
    </title>
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>" />
</head>
<body>
<app-root><div class="spinner"></div></app-root>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/OPSportfolio/dist/inline.bundle.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/OPSportfolio/dist/polyfills.bundle.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/OPSportfolio/dist/styles.bundle.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/OPSportfolio/dist/vendor.bundle.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/OPSportfolio/dist/main.bundle.js"></script></body>
</body>
</html>