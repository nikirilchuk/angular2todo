import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { WpPost } from '../interfaces/wp-post';

import { environment } from '../../environments/environment'

@Injectable()
export class HttpService {

	public apiurl: string;

  	constructor(private http: Http) {
  		this.apiurl = environment.apiUrl;
  	}

  	getPostsList():Observable<any> {
  		return this.http.get(`${this.apiurl}posts`).map((resp:Response)=>
	  		{
	            return resp;
	        });
  	}
  	getPost(id):Observable<any> {
  		return this.http.get(`${this.apiurl}posts/${id}`).map((resp:Response)=>
	  		{
	            return resp;
	        });
  	}
}
