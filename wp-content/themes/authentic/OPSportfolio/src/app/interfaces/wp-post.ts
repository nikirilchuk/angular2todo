export interface IRendered {
    rendered: string;
}

export interface WpPost {
    id: number;
    date: string;
    link: string;
    title: IRendered;
    content: IRendered;
    excerpt: IRendered;
    categories: number[];
    featured_media: number;
}
