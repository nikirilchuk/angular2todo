import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { PostListComponent } from '../../components/post-list/post-list.component';
import { PostListItemComponent } from '../../components/post-list-item/post-list-item.component';
import { SinglePostComponent } from '../../components/single-post/single-post.component';

import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  declarations: [
	  HomeComponent,
	  PostListComponent,
	  PostListItemComponent,
	  SinglePostComponent
  ]
})
export class HomeModule { }
