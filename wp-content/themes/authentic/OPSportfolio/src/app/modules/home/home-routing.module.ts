import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { SinglePostComponent } from '../../components/single-post/single-post.component';


const routes: Routes = [
 	{
	    path: 'home',
	    component: HomeComponent,
	    // canActivate: [MyUserGuard]
	},
	{
		path: 'post/:id',
	    component: SinglePostComponent,
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }
