import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { HttpService } from '../../services/http.service';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
  providers: [HttpService]
})
export class PostListComponent implements OnInit {

	private PostLists: {};

  	constructor(private httpservice: HttpService) { }

  	ngOnInit() {
  		this.httpservice.getPostsList().subscribe((data: Response) => {this.PostLists=data.json()});
  	}

}
