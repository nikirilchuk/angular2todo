import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss'],
  providers: [HttpService]
})
export class SinglePostComponent implements OnInit {
	private postData:any;
  	constructor(private httpservice:HttpService) {
  	}

  	ngOnInit() {
		this.httpservice.getPost(1).subscribe((data: Response) => {this.postData=data.json()});
  	}

}
